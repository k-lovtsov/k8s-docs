# Базовые функции

- [Официальная документация](https://kubernetes.io/ru/docs/home/)
- [Что такое Kubernetes](https://kubernetes.io/ru/docs/concepts/overview/what-is-kubernetes/)
- [Компоненты Kubernetes](https://kubernetes.io/ru/docs/concepts/overview/components/)
- [Шпаргалка по kubectl](https://kubernetes.io/ru/docs/reference/kubectl/cheatsheet/)
- [Шпаргалки по работе с k8s](https://kubernetes.io/docs/tasks/)

# Основные объекты

- [Pod](https://kubernetes.io/docs/concepts/workloads/pods/)
- [Service](https://kubernetes.io/docs/concepts/services-networking/service/)
- [Namespace](https://kubernetes.io/ru/docs/concepts/overview/working-with-objects/namespaces/)
- [Deployment](https://kubernetes.io/docs/concepts/workloads/controllers/deployment/)
- [DaemonSet](https://kubernetes.io/docs/concepts/workloads/controllers/daemonset/)
- [Job](https://kubernetes.io/docs/concepts/workloads/controllers/job/)
- [StatefulSet](https://kubernetes.io/docs/concepts/workloads/controllers/statefulset/)

# Minikube

- [Установка minukube](https://minikube.sigs.k8s.io/docs/start/)
- [Альтернатива в виде kind](https://kind.sigs.k8s.io/)
- [Play with Kubernetes](https://labs.play-with-k8s.com/)

# Яндекс Облако

- [Yandex Managed Service for Kubernetes](https://cloud.yandex.ru/ru/docs/managed-kubernetes/)
- [Установка Yandex Cloud CLI](https://cloud.yandex.ru/ru/docs/cli/operations/install-cli)
- [Получение kubeconfig](https://cloud.yandex.ru/ru/docs/cli/cli-ref/managed-services/managed-kubernetes/cluster/get-credentials)

# Полезные утилиты

- [Установка kubectl](https://kubernetes.io/ru/docs/tasks/tools/install-kubectl/)
- [Автодополнение для shell](https://kubernetes.io/ru/docs/reference/kubectl/cheatsheet/#kubectl-autocomplete)
- [K9S: Консольная утилита для удобной работы](https://habr.com/ru/companies/flant/articles/524196/)
